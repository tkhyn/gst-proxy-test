cmake_minimum_required(VERSION 3.15)
project(gst_proxy_test)

set(CMAKE_CXX_STANDARD 17)

add_executable(gst_proxy_test main.cpp)

if (WIN32)
  target_include_directories(gst_proxy_test PRIVATE
    include
    $ENV{GSTREAMER_1_0_ROOT_X86_64}include
    $ENV{GSTREAMER_1_0_ROOT_X86_64}include/gstreamer-1.0
    $ENV{GSTREAMER_1_0_ROOT_X86_64}include/glib-2.0
    $ENV{GSTREAMER_1_0_ROOT_X86_64}lib/glib-2.0/include
  )

  target_link_directories(gst_proxy_test PRIVATE
    $ENV{GSTREAMER_1_0_ROOT_X86_64}lib
  )

  target_link_libraries(gst_proxy_test PRIVATE
    gstreamer-1.0 gstrtspserver-1.0 gobject-2.0 glib-2.0 intl
  )
else()
  set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/.cmake")

  find_package(GStreamer)
  find_package(GLIB2)
  find_package(GObject)

  target_include_directories(gst_proxy_test PRIVATE ${GSTREAMER_INCLUDE_DIR} ${GLIB2_INCLUDE_DIR})
  target_link_libraries(gst_proxy_test PRIVATE ${GSTREAMER_LIBRARY} ${GOBJECT_LIBRARIES} ${GLIB2_LIBRARIES})
endif()
