#include <gst/gst.h>


int main(int argc, char* argv[]) {

    gst_init(&argc, &argv);

    GstElement *pipe1, *pipe2, *psink, *psrc;
    GstClock *clock;

    pipe1 = gst_parse_launch ("audiotestsrc ! proxysink name=psink", nullptr);
    psink = gst_bin_get_by_name (GST_BIN (pipe1), "psink");

    pipe2 = gst_parse_launch ("proxysrc name=psrc ! autoaudiosink", nullptr);
    psrc = gst_bin_get_by_name (GST_BIN (pipe2), "psrc");

    // Connect the two pipelines
    g_object_set (psrc, "proxysink", psink, NULL);

    // Both pipelines must agree on the timing information or we'll get glitches
    // or overruns/underruns. Ideally, we should tell pipe1 to use the same clock
    // as pipe2, but since that will be set asynchronously to the audio clock, it
    // is simpler and likely accurate enough to use the system clock for both
    // pipelines. If no element in either pipeline will provide a clock, this
    // is not needed.
    clock = gst_system_clock_obtain ();
    gst_pipeline_use_clock (GST_PIPELINE (pipe1), clock);
    gst_pipeline_use_clock (GST_PIPELINE (pipe2), clock);
    g_object_unref (clock);

    // This is not really needed in this case since the pipelines are created and
    // started at the same time. However, an application that dynamically
    // generates pipelines must ensure that all the pipelines that will be
    // connected together share the same base time.
    gst_element_set_base_time (pipe1, 0);
    gst_element_set_base_time (pipe2, 0);

    gst_element_set_state (pipe1, GST_STATE_PLAYING);
    gst_element_set_state (pipe2, GST_STATE_PLAYING);

    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipe1), GST_DEBUG_GRAPH_SHOW_ALL, "pipe1");
    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipe2), GST_DEBUG_GRAPH_SHOW_ALL, "pipe2");

    // Wait until error or EOS
    GstBus* bus = gst_element_get_bus(pipe1);

    GstMessage* msg = gst_bus_timed_pop_filtered(
        bus, GST_CLOCK_TIME_NONE,
        static_cast<GstMessageType>(GST_MESSAGE_ERROR | GST_MESSAGE_EOS)
    );

    if (msg) {
        gst_message_unref (msg);
    }

    // Free resources
    gst_object_unref(bus);

    gst_element_set_state(pipe1, GST_STATE_NULL);
    gst_element_set_state(pipe2, GST_STATE_NULL);

    gst_object_unref(pipe1);
    gst_object_unref(pipe2);

}
