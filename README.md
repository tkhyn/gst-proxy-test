# gst-proxy-test

This sample highlights an issue with the gstproxy plugin on Windows. It works fine on Linux (the
test sound can be heard) but fails on Windows (no sound can be heard).

## Prerequisites

- CMake 3.15 (can certainly work with older version, just change `cmake_minimum_required`)
- C/C++ compiler

### Linux (Ubuntu / Debian)

Tested with GCC 7.4 on Linux Mint 19.3 (based on Ubuntu 18.04 LTS)

```bash
sudo apt install libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio
```

### Windows (x64)

Tested on Windows 10 1909 build 18363, with MSVC 19.24.28315.0 compiler.

- Install gstreamer 1.0 runtime and dev packages in the default location (`C:\gstreamer`)
- Check or set the `GSTREAMER_1_0_ROOT_X86_64` to make it point to `C:\gstreamer\1.0\x86_64\`
- Add `C:\gstreamer\1.0\x86_64\bin` to `PATH`


## Build

```bash
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

(on Windows, opening the folder in VS2019 will also work)

## Run

```bash
./gst_proxy_test
```

(on Windows, launching the 'gst_proxy_test.exe' target from VS2019 will do the job)

## Generate debug graphs

```bash
mkdir graphs
export GST_DEBUG_DUMP_DOT_DIR=$PWD/graphs
./gst_proxy_test --gst-enable-gst-debug
```
